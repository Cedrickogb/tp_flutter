
import 'package:flutter/material.dart';
import 'package:tp_app/screens/home.dart';



import 'package:tp_app/screens/home.dart';
import 'package:tp_app/screens/login.dart';
import 'package:tp_app/screens/stetting.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() =>
      _MyAppState();
}

class _MyAppState extends State {
  int _selectedTab = 0;

  List _pages = [
    Center(
      child: const HomeScreen(),
    ),
    Center(
      child: const LoginPage(),
    ),
    Center(
      child: const SettingPage(),
    ),
  ];

  _changeTab(int index) {
    setState(() {
      _selectedTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: _pages[_selectedTab],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedTab,
          onTap: (index) => _changeTab(index),
          selectedItemColor: Color.fromARGB(255, 12, 170, 67),
          unselectedItemColor: Colors.grey,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(
                icon: Icon(Icons.login), label: "Login"),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), label: "Settings"),
          ],
        )  ),
    );
  }
}