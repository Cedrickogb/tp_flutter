import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
          ),
          body:  SingleChildScrollView(
            child: Center(
              child: Column (
              children: [
                Row (children: [ 
                  Expanded(flex:4 , child: Image.asset("assets/images/15.jpg"),),
                  Expanded(flex:4 , child: Image.network("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/800px-Image_created_with_a_mobile_phone.png"),)
                  ],
                ),
                const Text (style: TextStyle(fontSize: 35, fontWeight:FontWeight.bold),
                "OGOUBIYI"),
                const Text (style: TextStyle(fontSize: 20),
                 "Cédrick")
              ],
            )) 
          )
      )
    );
  }
}