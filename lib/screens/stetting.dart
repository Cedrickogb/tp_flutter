import 'package:flutter/material.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  // Les couleurs à afficher dans la liste déroulante
  final List<Color> colors = [    Colors.blue,    Colors.red,    Colors.green,    Colors.yellow,    Colors.purple,  ];

  // La couleur sélectionnée
  Color selectedColor = Colors.blue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: selectedColor, // Couleur de l'appBar
        title: const Text('Settings'),
      ),
      body: Center(
        child: DropdownButton<Color>(
          value: selectedColor,
          onChanged: (Color? value) {
            if (value != null) {
              setState(() {
                selectedColor = value;
              });
            }
          },
          items: colors.map((Color color) {
            return DropdownMenuItem<Color>(
              value: color,
              child: Container(
                width: 50,
                height: 50,
                color: color,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}